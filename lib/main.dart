import 'package:flutter/material.dart';
import 'package:twillio_mvp/chats.user.ui.dart';
import 'package:twillio_mvp/login.dart';
import 'package:twillio_mvp/twilio.implementation.dart';

import 'channel.example.dart';

  void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: PlatformChannel(),
    );
  }
}

//class TwillioImplementationUi extends StatefulWidget {
//  @override
//  _TwillioImplementationUiState createState() =>
//      _TwillioImplementationUiState();
//}
//
//class _TwillioImplementationUiState extends State<TwillioImplementationUi> {
//  TwilioImplmentation _twilioImplmentation;
//  var data;
//  var status;
//  bool isLoading = true;
//  TextEditingController controller;
//
//  @override
//  void initState() {
//    super.initState();
//    _twilioImplmentation = TwilioImplmentation();
//    controller=TextEditingController();
//    init();
//  }
//
//  void init() async {
//    status = await _twilioImplmentation.initTwilio();
//    setState(() {
//      isLoading = false;
//    });
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        title: Text("Twilio Implementation"),
//      ),
//      body: Center(
//          child: Column(
//        mainAxisAlignment: MainAxisAlignment.center,
//        children: <Widget>[
////          TextField(
////            decoration: InputDecoration(hintText: "Enter name"),
////            controller:  controller,
////          ),
////          RaisedButton(
////
////            onPressed: () {
////              setState(() {isLoading=true;});
////              _twilioImplmentation.changeName(controller.text);
////              setState(() {isLoading=false;});
////            },
////            child: Text("Chnage name"),
////          ),
//
//          Text(isLoading ? "Loading..." : "not loading"),
//          Text("Status: ${status}"),
//          Text("Data: ${data}"),
//          RaisedButton(
//            onPressed: () {
//             init();
//            },
//            child: Text("Initiate"),
//          ),
//          RaisedButton(
//            onPressed: () {
//              _twilioImplmentation.getMessages();
//            },
//            child: Text("Messages"),
//          ),
//          RaisedButton(
//            onPressed: () async {
//              isLoading = true;
//              setState(() {});
//              data = await _twilioImplmentation.sendMessage();
//              print(data);
//              setState(() {
//                isLoading = false;
//              });
//            },
//            child: Text("Send Messages"),
//          ),
//        ],
//      )),
//    );
//  }
//}
