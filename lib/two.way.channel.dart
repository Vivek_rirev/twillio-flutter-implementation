import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main(args) => runApp(TwoWayTest());

class TwoWayTest extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TwoWayTestState();
  }
}

class TwoWayTestState extends State<TwoWayTest> {
  static const String _CHANNEL_NAME = "CHANNEL";
  static const String _STREAM_METHOD_NAME = "STREAM_METHOD";

  void startStream() async {
    final eventChannel = EventChannel(_CHANNEL_NAME);
    eventChannel
        .receiveBroadcastStream()
        .listen(onStreamData, onError: onStreamError);
    final Uint8List encoded = utf8.encoder.convert("Hello");
//ByteData byteData=encoded.buffer.asByteData();
    var buffer=new Uint8List(8).buffer;
    var bData=new ByteData.view(buffer);
    bData.setFloat32(0,3.20);

    eventChannel.binaryMessenger
        .send(_CHANNEL_NAME,bData );
  }

  onStreamData(dynamic event) {
    String normalizedEvent = event.toLowerCase();
    print("Event Received: $normalizedEvent");
  }

  onStreamError(A) {
    print("Error");
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text("Method channel"),
      ),
      body: Column(
        children: <Widget>[
          RaisedButton(
            onPressed: () {
              startStream();
            },
            child: Text("Stream method"),
          )
        ],
      ),
    ));
  }
}
