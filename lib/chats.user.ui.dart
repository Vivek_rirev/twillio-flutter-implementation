import 'package:flutter/material.dart';
import 'package:twillio_mvp/chats.msgs.ui.dart';

import 'models.dart';

class ChatUserUI extends StatefulWidget {
  final String name;

  ChatUserUI(this.name);

  @override
  _ChatUserUIState createState() => _ChatUserUIState();
}

class _ChatUserUIState extends State<ChatUserUI> {
  String userName;

  @override
  void initState() {
    super.initState();
    userName = widget.name;
  }

  startChat(context, String name) {
    ChatUser chatUser = ChatUser(userName, name);
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => ChatMessageUi(chatUser)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Chat"),
      ),
      body: Column(
        children: <Widget>[
          Text("Welcome $userName"),
          RaisedButton(
            onPressed: () {
              startChat(context, "Vivek");
            },
            child: Text("Vivek"),
          ),
          RaisedButton(
            onPressed: () {
              startChat(context, "Prikesh");
            },
            child: Text("Prikesh"),
          ),
          RaisedButton(
            onPressed: () {
              startChat(context, "Malav");
            },
            child: Text("Malav"),
          )
        ],
      ),
    );
  }
}
