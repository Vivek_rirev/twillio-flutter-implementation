import 'package:flutter/services.dart';
import 'dart:convert';

class TwilioImplmentation {
  static const platform = const MethodChannel('TwilioImplementation');

  Future<String> initTwilio(String user1,String user2) async {
    final String result = await platform.invokeMethod("init",{"user1":user1,"user2":user2});
    print(result);
    return result;
  }


  Future<String> getMessages() async {
//    await initTwilio();
    final result = await platform.invokeMethod("getMessages");
   return result;
  }

  Future<String> sendMessage(String text) async {
    final result =
        await platform.invokeMethod("sendMessage", {"message": text});
    print(result);
    return result.toString();
  }
}
TwilioImplmentation twilioImplmentation=TwilioImplmentation();
