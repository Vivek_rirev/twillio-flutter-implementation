import 'package:flutter/material.dart';

import 'chats.user.ui.dart';

class LoginUi extends StatefulWidget {
  @override
  _LoginUiState createState() => _LoginUiState();
}

class _LoginUiState extends State<LoginUi> {
  openChats(String name){
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => ChatUserUI(name)));
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("USERS"),),
      body: Column(
        children: <Widget>[
          RaisedButton(
            child: Text("Vivek"),
            onPressed: (){
              openChats("Vivek");
            },
          ),
          RaisedButton(
            child: Text("Prikesh"),
            onPressed: (){
              openChats("Prikesh");
            },
          ),
          RaisedButton(
            child: Text("Malav"),
            onPressed: (){
              openChats("Malav");
            },
          ),

        ],
      ),
    );
  }
}
