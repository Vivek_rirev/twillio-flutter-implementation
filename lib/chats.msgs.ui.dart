import 'package:flutter/material.dart';
import 'package:twillio_mvp/models.dart';
import 'package:twillio_mvp/twilio.implementation.dart';

class ChatMessageUi extends StatefulWidget {
  final ChatUser user;

  ChatMessageUi(this.user);

  @override
  State<StatefulWidget> createState() => ChatMessageUiState();
}

class ChatMessageUiState extends State<ChatMessageUi> {
  String messages;

  bool isOnline;
  String connectionStatus;
  String userStatus;
  bool isLoading;
  TextEditingController controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isOnline = false;

    controller = TextEditingController();
    initChat();
  }

  void initChat() async {
    isLoading = true;
    setState(() {});
    connectionStatus = await twilioImplmentation.initTwilio(
        widget.user.userName1, widget.user.userName2);
    messages = await twilioImplmentation.getMessages();

    isLoading = false;
    setState(() {});
  }

  void sendMessage() async {
    isLoading = true;
    setState(() {});
    userStatus = await twilioImplmentation.sendMessage(controller.text);
    messages = await twilioImplmentation.getMessages();
    isLoading = false;
    controller.text = "";
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.user.userName1),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Flexible(
                child: Text("Messages: ${messages == null ? "" : messages}")),
            SizedBox(
              height: 16,
            ),
            Text(
                "Connection Status : ${connectionStatus == null ? "undefined" : connectionStatus}"),
            SizedBox(
              height: 16,
            ),
            Text(
                "User Status : ${connectionStatus == null ? "undefined" : userStatus}"),
            SizedBox(
              height: 16,
            ),
            TextField(
              controller: controller,
              decoration: InputDecoration(hintText: "Enter message"),
            ),
            SizedBox(
              height: 24,
            ),
            SizedBox(
              width: double.infinity,
              child: isLoading
                  ? Center(child: CircularProgressIndicator())
                  : RaisedButton(
                      child: Text("Send"),
                      onPressed: () {
                        sendMessage();
                      },
                    ),
            )
          ],
        ),
      ),
    );
  }
}
