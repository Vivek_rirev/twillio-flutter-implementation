package com.rirev.twillio_mvp

import android.content.*
import android.os.BatteryManager
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.os.Handler
import android.util.Log
import androidx.annotation.NonNull
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.*
import io.flutter.plugin.common.EventChannel.EventSink
import io.flutter.plugins.GeneratedPluginRegistrant
import java.nio.ByteBuffer


class MainActivity : FlutterActivity() {
    private val CHANNEL: String = "TwilioImplementation"
    private val DYNAMIC_CHANNEL: String = "CHANNEL"
    private val MEHTOD_TO_CALL_IN_FLUTTER: String = "FlutterMethod"
    private var twillioImplementation: TwillioImplementation? = null
    private val BATTERY_CHANNEL = "samples.flutter.io/battery"
    private val CHARGING_CHANNEL = "samples.flutter.io/charging"

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);
        MethodChannel(flutterEngine.dartExecutor, CHANNEL).setMethodCallHandler { call, result ->

            if (call.method == "init") {
                val user1: String? = call.argument<String>("user1")
                val user2: String? = call.argument<String>("user2")
                if (user1 != null && user2 != null)
                    twillioImplementation = TwillioImplementation(context, result, user1, user2)
                else result.success("ERROR")

            } else if (call.method == "getMessages") {
                twillioImplementation?.getMessagesInString(object : Callback {
                    override fun onSuccess(callBackResult: String) {
                        result.success(callBackResult);
                    }
                })
            } else if (call.method == "sendMessage") {

                call.argument<String>("message")?.let { twillioImplementation?.sendMessage(it, result) }
            } else if (call.method == "changeName") {
                call.argument<String>("name")?.let { twillioImplementation?.changeName(it, result) }
            }
        }
        EventChannel(flutterEngine.dartExecutor, DYNAMIC_CHANNEL).setStreamHandler(DynamicStreamHandler())

        startCallingFlutterMethod()


        EventChannel(flutterEngine.dartExecutor, CHARGING_CHANNEL).setStreamHandler(
                object : EventChannel.StreamHandler {
                    private var chargingStateChangeReceiver: BroadcastReceiver? = null
                    override fun onListen(arguments: Any, events: EventSink) {
                        chargingStateChangeReceiver = createChargingStateChangeReceiver(events)
                        registerReceiver(
                                chargingStateChangeReceiver, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
                    }

                    override fun onCancel(arguments: Any) {
                        unregisterReceiver(chargingStateChangeReceiver)
                        chargingStateChangeReceiver = null
                    }
                }
        )

        MethodChannel(flutterEngine.dartExecutor, BATTERY_CHANNEL).setMethodCallHandler { call, result ->
            if (call.method.equals("getBatteryLevel")) {
                var batteryLevel: Int = getBatteryLevel();
                if (batteryLevel != -1)
                    result.success(batteryLevel)
                else result.error("UNAVAILABLE", "Battery level not available", null)
            } else result.notImplemented()
        }
    }

    private fun getBatteryLevel(): Int {
        if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
            val batteryManager = getSystemService(Context.BATTERY_SERVICE) as BatteryManager
            return batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
        } else {
            val intent = ContextWrapper(applicationContext).registerReceiver(null, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
            return intent!!.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) * 100 / intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
        }
    }

    private fun createChargingStateChangeReceiver(events: EventSink): BroadcastReceiver {
        return object : BroadcastReceiver() {
            override fun onReceive(p0: Context?, p1: Intent?) {
                val status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1)

                if (status == BatteryManager.BATTERY_STATUS_UNKNOWN) {
                    events.error("UNAVAILABLE", "Charging status unavailable", null)
                } else {
                    val isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL
                    events.success(if (isCharging) "charging" else "discharging")
                }
            }
            //            fun onReceive(context: Context, intent: Intent) {

//            }
        }
    }

    fun startCallingFlutterMethod() {
        var channel = BasicMessageChannel<String>(Hello(), MEHTOD_TO_CALL_IN_FLUTTER, StringCodec.INSTANCE)
        Handler().postDelayed({
            channel.send("Native Call")
        }, 1000)
    }

}

class DynamicStreamHandler : EventChannel.StreamHandler {
    override fun onListen(arguments: Any?, events: EventChannel.EventSink?) {
        Log.d("TAG", "listen")

    }

    override fun onCancel(arguments: Any?) {
        Log.d("TAG", "cancel")
    }

}

class Hello : BinaryMessenger {
    override fun setMessageHandler(channel: String, handler: BinaryMessenger.BinaryMessageHandler?) {

    }

    override fun send(channel: String, message: ByteBuffer?) {
        Log.d("TAG", "send1")
    }


    override fun send(channel: String, message: ByteBuffer?, callback: BinaryMessenger.BinaryReply?) {
        Log.d("TAG", "send2")
    }
}
