package com.rirev.twillio_mvp

import com.twilio.chat.Message
import org.json.JSONArray
import org.json.JSONObject

object TwillioUtil {

    fun getMessagesInJson(messages: ArrayList<Message>): JSONArray {
        var jsonArray: JSONArray = JSONArray()

        for (eachMessage: Message in messages) {

            jsonArray.put(eachMessage.messageBody)
        }
        return jsonArray;
    }

}