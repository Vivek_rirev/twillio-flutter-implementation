package com.rirev.twillio_mvp

import android.content.Context
import android.provider.Settings
import android.util.Log
import com.koushikdutta.async.future.FutureCallback
import com.koushikdutta.ion.Ion
import com.twilio.chat.*
import io.flutter.plugin.common.MethodChannel

class TwillioImplementation(private var mContext: Context, private var result: MethodChannel.Result, private var user1: String, private var user2: String) {
    val SERVER_TOKEN_URL: String = "https://bazaar-ant-7477.twil.io/chat-token"
    private val mMessages = ArrayList<Message>()

    var DEFAULT_CHANNEL_NAME: String = "general1"
    val TAG: String = "TwilioChat"

    // Update this identity for each individual user, for instance after they login
    private var mChatClient: ChatClient? = null
    private var mGeneralChannel: Channel? = null

    init {

        if (user1 > user2) {
            DEFAULT_CHANNEL_NAME = user1 + user2
        } else
            DEFAULT_CHANNEL_NAME = user2 + user1
        retriveAccessTokenFromServer()
    }

    private val mChatClientCallback: CallbackListener<ChatClient> = object : CallbackListener<ChatClient>() {
        override fun onSuccess(chatClient: ChatClient) {
            mChatClient = chatClient
            Log.d(TAG, chatClient.toString())

            loadChannels()
//            Log.d(MainActivity.TAG, "Success creating Twilio Chat Client")
        }

        override fun onError(errorInfo: ErrorInfo) {
            result.success(errorInfo.message)
//            Log.e(MainActivity.TAG, "Error creating Twilio Chat Client: " + errorInfo.message)
        }
    }

    fun changeName(name: String, result: MethodChannel.Result) {
        user1 = name;
        result.success(user1)
    }

    fun sendMessage(message: String, result: MethodChannel.Result) {
        val options = Message.options().withBody(message)
        if (mGeneralChannel == null)
            result.success("channel is null")
        mGeneralChannel?.messages?.sendMessage(options, object : CallbackListener<Message>() {
            override fun onSuccess(p0: Message?) {
//                p0?.let { mMessages.add(it) };
                result.success("Message send : " + message)
            }

        })

    }

    private val mDefaultListener: ChannelListener = object : ChannelListener {
        override fun onMemberDeleted(p0: Member?) {

        }

        override fun onTypingEnded(p0: Channel?, p1: Member?) {

        }

        override fun onMessageAdded(p0: Message?) {
            if (p0 != null) {
                mMessages.add(p0)
            };
        }

        override fun onMessageDeleted(p0: Message?) {
            mMessages.remove(p0);
        }


        override fun onMemberAdded(p0: Member?) {
        }

        override fun onTypingStarted(p0: Channel?, p1: Member?) {
        }

        override fun onSynchronizationChanged(p0: Channel?) {
        }

        override fun onMessageUpdated(p0: Message?, p1: Message.UpdateReason?) {
        }

        override fun onMemberUpdated(p0: Member?, p1: Member.UpdateReason?) {
        }
    }

    fun retriveAccessTokenFromServer() {
        val deviceID: String = Settings.Secure.getString(mContext.contentResolver, Settings.Secure.ANDROID_ID)
        val tokenURL: String = SERVER_TOKEN_URL + "?device" + deviceID + "&identity=" + user1
        Ion.with(mContext).load(tokenURL).asJsonObject().setCallback(FutureCallback { e, result ->
            if (e == null) {
                val accessToken: String = result.get("token").asString
                val builder: ChatClient.Properties.Builder = ChatClient.Properties.Builder()

                ChatClient.create(mContext, accessToken, builder.createProperties(), mChatClientCallback)
            } else {
                // do something
                Log.d(TAG, e.message)
            }
        })
    }

    fun loadChannels() {
        mChatClient?.channels?.getChannel(DEFAULT_CHANNEL_NAME, object : CallbackListener<Channel>() {
            override fun onSuccess(channel: Channel?) {
                try {
                    Log.d(TAG, System.currentTimeMillis().toString())
                    Thread.sleep(1000)
                    Log.d(TAG, System.currentTimeMillis().toString())
                    if (channel != null) {
                        Log.d(TAG, "channel is not null")
                        try {
                            var member: Member = channel.members.getMember(user1);

                            if (member == null) {
                                joinChannel(channel)
                            } else {
                                mGeneralChannel = channel
                                mGeneralChannel?.addListener(mDefaultListener)
                                result.success("Connected")
                            }
                        } catch (e: Exception) {
                            joinChannel(channel);
                        }


                    } else {
                        Log.d(TAG, "Creating channel")
                        createChannel();

                    }
                } catch (e: Exception) {
                    print(e);
                    result.success(e.message);
                }
            }

            override fun onError(errorInfo: ErrorInfo) {
                Log.e(TAG, "Error retrieving channel: " + errorInfo.message)
                createChannel()

            }
        })
    }

    fun joinChannel(channel: Channel?) {

        Log.d(TAG, "Joining Channel: " + channel!!.uniqueName)
        channel.join(object : StatusListener() {
            override fun onSuccess() {
                mGeneralChannel = channel;
                Log.d(TAG, "Joined " + DEFAULT_CHANNEL_NAME + " channel")
                mGeneralChannel?.addListener(mDefaultListener)

                result.success(" Connected as new ")
            }

            override fun onError(errorInfo: ErrorInfo?) {
                super.onError(errorInfo)
                Log.d(TAG, "Can't join default channel .. Trying again")

                createChannel()
            }
        })

    }

    fun createChannel() {
        mChatClient?.channels?.createChannel(DEFAULT_CHANNEL_NAME, Channel.ChannelType.PUBLIC, object : CallbackListener<Channel>() {
            override fun onSuccess(p0: Channel?) {
                if (p0 != null) {
                    Log.d(TAG, "Channel created")
                    p0.setUniqueName(DEFAULT_CHANNEL_NAME, object : StatusListener() {
                        override fun onSuccess() {
                            Log.d(TAG, "unique name created")

                            joinChannel(p0)
                        }
                    })

                }
            }

            override fun onError(errorInfo: ErrorInfo?) {
                super.onError(errorInfo)
                Log.d(TAG, errorInfo.toString())
            }
        })
    }

    fun getMessagesInString(callback: Callback) {
        mGeneralChannel?.getMessagesCount(object : CallbackListener<Long>() {
            override fun onSuccess(p0: Long?) {
                Log.d(TAG, p0.toString())
                mGeneralChannel?.messages?.getMessagesAfter(0, p0!!.toInt(), object : CallbackListener<List<Message>>() {
                    override fun onSuccess(p1: List<Message>?) {
                        Log.d(TAG, p1!!.size.toString())
                        var msgs: String = "";
                        for (ms: Message in p1) {
                            Log.d(TAG, ms.author + ": " + ms.messageBody)
                            msgs = msgs + "," + ms.author + ": " + ms.messageBody;
                        }
                        callback.onSuccess(msgs)
                    }

                    override fun onError(errorInfo: ErrorInfo?) {
                        super.onError(errorInfo)
                    }
                })
            }
        })
    }


}

interface Callback {
    fun onSuccess(result: String)
}